#!/usr/bin/env bash

clear
markdownSlidesBaseDir=$(dirname "$0")

ORIGIN=`pwd`
PATH_FILE=""
FILENAME=""
FILENAMENOEXT=""
CSSFILE="./markdownAssignmentPdf.css"
CHROME_COMMAND="/usr/bin/google-chrome"

##  $1: PAth to the file in call
##  $2: Execution path
function getFilePath() {
#  echo -e "getFilePAth:"
#  echo -e "     Primer parámetro: $1 "
#  echo -e "     Segundo parámetro: $2"
  firstCharacter="${1:0:1}"

  if [ "$firstCharacter" == "/" ]; then
    echo -e "Path completo: $1"
    PATH_FILE=${1%/*}
    FILENAME=${1##*/}
    FILENAMENOEXT="${FILENAME%.*}"
  else
    echo -e "Path completo: $2/$1"
    completo="$2/$1"
    PATH_FILE=${completo%/*}
    FILENAME=${completo##*/}
    FILENAMENOEXT="${FILENAME%.*}"
  fi
#  echo -e "getFilePath:"
#  echo -e "     PATH:           $PATH"
#  echo -e "     FILENAME:       $FILENAME"
#  echo -e "     FILENAMENOEXT:  $FILENAMENOEXT"
}

function help() {
  echo Se debe pasar como parámetro como mínimo el path al fichero que quiere transformarse de markdown a revealjs slides:
  echo       ej.  $0 ./presentacion.md
  echo       ej.  $0 /home/usuario/presentacion.md
  exit
}

## MAIN


if [ "x$1" != "x" ]; then

  if [ ! -f "$1" ]; then
    help
  fi

  getFilePath "$1" "`pwd`"
  FILENAMENOEXT="${FILENAME%.*}"

  echo -e "Transforming "
  echo -e "   markdown file: $PATH_FILE/$FILENAME "
  echo -e "   into Pdf $PATH_FILE/${FILENAMENOEXT}_assignmentPdf.html"
  echo -e " ................"

  # tmpfile="${PATH_FILE}/${FILENAMENOEXT}_temp_${RANDOM}.html"
  echo -e "Temp file: ${tmpfile}"
  tmpfile="/tmp/${FILENAMENOEXT}_${RANDOM}.html"

  /usr/bin/pandoc  -f markdown -t html5 -s  \
  --top-level-division=chapter --metadata pagetitle="${FILENAMENOEXT}" --css=$markdownSlidesBaseDir/$CSSFILE --highlight-style=tango \
    -o "$tmpfile" "$PATH_FILE/$FILENAME" $2

  if [ ! -f "$tmpfile" ]; then
    echo -e "No se generó correctamente el fichero $tmpfile"
  else
    /usr/bin/google-chrome --headless --no-margins --virtual-time-budget=1337  --disable-gpu --print-to-pdf="${PATH_FILE}/${FILENAMENOEXT}_assignmentPdf.pdf" "${tmpfile}"
    if [ ! -f "$PATH_FILE/${FILENAMENOEXT}_assignmentPdf.pdf" ]; then
      echo -e "No se generó correctamente el fichero '$PATH_FILE/${FILENAMENOEXT}_assignmentPdf.pdf' a partir del temporal '${tmpfile}'"
      rm -iv "${tmpfile}"
    else
      rm -f "${tmpfile}"
      # rm -f "${tmpfile}"
    fi
  fi

  echo -e "HECHO."

##    --self-contained
else
  help

fi
