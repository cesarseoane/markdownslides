#!/bin/bash
# usage: ./script.sh file.html
#echo EJECUTANDO image-self-contained
#echo aa$*aa

FILEPATH=""
FILENAME=""
FILENAMENOEXT=""
FILEEXTENSION=""
NAMEADDITION="_self-contained"

#echo $PATH

function getFilePath() {
#  echo -e "getFilePAth:"
#  echo -e "     Primer parámetro: $1 "
#  echo -e "     Segundo parámetro: $2"
  firstCharacter="${1:0:1}"

  if [ "$firstCharacter" == "/" ]; then
#    echo -e "Path completo: \"$1\""
    FILEPATH=${1%/*}
    FILENAME=${1##*/}
    FILENAMENOEXT="${FILENAME%.*}"
    FILEEXTENSION="${FILENAME##*.}"
  else
#    echo -e "Path completo: \"$2/$1\""
    completo="$2/$1"
    FILEPATH=${completo%/*}
    FILENAME=${completo##*/}
    FILENAMENOEXT="${FILENAME%.*}"
    FILEEXTENSION="${FILENAME##*.}"
  fi
 echo -e "GetFilePath:"
 echo -e "     FILEPATH:           $FILEPATH"
 echo -e "     FILENAME:       $FILENAME"
 echo -e "     FILENAMENOEXT:  $FILENAMENOEXT"
 echo -e "     FILEEXTENSION:  $FILEEXTENSION"
}


getFilePath "$1" "`pwd`"

# echo "Antes de mktemp"

tmpdir=$(dirname $(mktemp -u))/images_temp
mkdir $tmpdir 2> /dev/null;


tmpfile=$(mktemp)
cp "$1" $tmpfile
cp $tmpfile ${tmpfile}_1; echo "tempFile: " ${tmpfile}
#cat $tmpfile | grep "data:"
echo ""
echo "    Imágenes encontradas:"
sed -n "/<img/s/.*src[\\]\?=[\"']\([^\"^']*\)[\"'].*/\1/p" $tmpfile
echo ""

for i in `sed -n "/<img/s/.*src[\\]\?=[\"']\([^\"^']*\)[\"'].*/\1/p" $tmpfile`;
do

    fullfilename="";
    echo ""
    echo "--------------------- SRC: ${i:0:40} --------------------------------------------------"
    # echo ""
    # echo "#### i vale: ###"
    # echo $i
    # echo ""

    cleanI=$(echo $i|sed -e 's/%20/ /g');
    if [[ $i == *"data:"*";base64"* ]];
    then
        echo "--   ## Already a base64 image: \"${i:0:40}\"## ";
        continue;
    else
      echo "--   ## No base64 Image ##";
      RELATIVEPATH_IMAGE=$(pwd)/$cleanI
      #echo "## -- ##  RELATIVEPATH_IMAGE= ${RELATIVEPATH_IMAGE}"
      if [ -f "${RELATIVEPATH_IMAGE}" ];
      then
        fullfilename=${RELATIVEPATH_IMAGE};
        echo "--   ## IS    local file. RelativePath to Image: ${fullfilename}";
      else
        echo "--   ## NOT a local file (relative path would be  ${RELATIVEPATH_IMAGE}. pwd: $(pwd) )";
      fi
      FULLPATH_IMAGE="${cleanI}";
      #echo "## ABSOLUTE ##  FULLPATH_IMAGE= ${FULLPATH_IMAGE}"
      if [ "a${fullfilename}" == "a" ] && [ -f "${FULLPATH_IMAGE}" ];
      then
        fullfilename=${FULLPATH_IMAGE};
        echo "--   ## IS a local file. Absolute Local path ${fullfilename}";
      else
        echo "--   ## NOT a local file (absolute path would be ${FULLPATH_IMAGE}. pwd: $(pwd) )";
      fi
      if [ "a${fullfilename}" == "a" ];
      then
        # echo "   ##  Cleaning images_temp directory: $tmpdir";
        rm $tmpdir/*;

        echo -n "--   ## URL ## Downloading the image .......";
        wget --timeout=3 --tries=3 -P $tmpdir/ $i 2> /dev/null;
        fullfilename="${tmpdir}/`ls $tmpdir`"
        echo "--        done ;)" 
      fi

      echo "--   ## TempFile ##: #$fullfilename#"
      if [ "a$fullfilename" == "a" ];
      then
        echo "--   ## IMAGE ERROR !! ## $i no se ha detectado ni como local ni como URL válida";
        continue;
      fi
      #echo "######### convert the image for size saving";
      #convert -quality 70 `echo ${i##*/}` `echo ${i##*/}`.temp;
      mimeType=$(file --mime-type "${fullfilename}" | cut -d ':' -f 2 | awk '{$1=$1};1' )
      echo "--   ## MimeType ## MimeType: ##$mimeType## (File: ##${fullfilename}##)";
      # read -t 5 -p "I am going to wait for 5 seconds only ..."

      # mimeType="image/png"
      # k="`echo "data:${mimeType};base64,"`$(base64 -w 0 $tmpdir/`echo ${i##*/}`)";
      #echo "   ## K2 ## ${k2:0:40} ... ${k2: -40}"
      k="data:${mimeType};base64,"$(base64 -w 0 "$fullfilename");
      # echo "--   ## K ## ${k:0:40} ... ${k: -40}"
      #echo "--   ## K ## ${k}"
      
      # Replacing = by 0x3D because the '=' interferes with Moodle Gift answer simbol
      # The = character is very common at the end of Base64 encoded (Reference: https://en.wikipedia.org/wiki/Base64)
      # Replacing by %3d instead of 0x3d causes problems in quizToPdf script used to get a PDF of each student quiz automatically from Moodle
      k="${k//[=]/0x3D}"
      echo "--   ## K with = replaced ## ${k:0:40} ... ${k: -40}"
      
      echo "s|$i|$k|" > $tmpdir/foo.sed
      sed -f $tmpdir/foo.sed "$tmpfile" >  $tmpdir/temp.html
      echo "--   ## Replace string in temp ##  $tmpdir/temp.html";

      # echo "--   ## Replacing final ## Replace final $tmpfile by temp $tmpdir/temp.html";
      rm -rf $tmpfile && mv  $tmpdir/temp.html $tmpfile;

      # sleep 5;
    fi

done;
echo ""
echo "Replacing original file \"$FILEPATH/$FILENAMENOEXT$NAMEADDITION.$FILEEXTENSION\"";
mv $tmpfile  "$FILEPATH/$FILENAMENOEXT$NAMEADDITION.$FILEEXTENSION"
