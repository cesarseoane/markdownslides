# README #

This projects try to simplify the transformation from markdown to reveal slides used by César Seoane.

### What is this repository for? ###

* Starting from https://github.com/asanzdiego/markdownslides
* Try to simplify the use that Adolfo makes from pandoc:
    * Eliminating the need of export directory
    * Allowing to execute the command with absolute and relative paths
    * Leaving the generated file in the same directory
    * Keep original templates as simple and as original as possible.
* Add new features:
    * **image-sel-contained.sh** Transform all image src parameter to the same image but base64 encoded. It's possible to apply this script to any kind of file. For example the gift Moodle question format

### Work needed ###

* Create a custom menu like markdown with the four submenu options instead of scripts.
* The script is working with the 3.9.2 version of revealjs and 2.5 version of Pandoc. There are newer versions that may have interesting features.

### Installation ###

What I think is the best option is to clone the repository into /opt directory and make the clone directory part of the path

To do /opt/markdownslides part ot your path, add to the end of your ~/.bashrc file:

    `export PATH=/opt/markdownslides:$PATH`

The pandoc software will be needed. In [this URL](https://pandoc.org/installing.html) you can see how to install it.

You can install it in Ubuntu with `apt install pandoc`. It will install the version 2.5 in Ubuntu 20.04

### Uso ###

Once it is install and in the system path, it can be executed like this:

* `markdownSlices fichero.md` It will generate one html file called fichero_revealjs.html íncluding the revealjs presentation.
* `markdownSlices fichero.md --self-contained ` It will generate a presentation with revealjs similar to previouse command, but including external images as data URIs and CSS and JS inside the html file.
* `markdownAssignment fichero.md` It will generate a formated HTML. perfect for a short document like an assignment or small explanation.
* `markdownAssignment fichero.md --self-contained ` Same as before but images will be included in the document as data URI and CSS and JS will be included in the HTML file.
* `image-sel-contained fichero` It will generate a file with same name and extension but including at the end of the name _self-contained. It will have all the images src with base64 encoded image data.

### Uso scripts Nautilus ###

In nautilusSctips folder, there are 5 scripts to add to Nautilus file explorer in order to lauch easily 4 options as you can see in the picture.

![](./nautilusScripts.png)

This files should be copied to ~/.local/share/nautilus/scripts and should have execution permissions
