#! /bin/bash

clear
markdownSlidesBaseDir=$(dirname "$0")

FILEPATH=""
FILENAME=""
FILENAMENOEXT=""
TEMPLATE="template.revealjs"
echo "Template in: $markdownSlidesBaseDir/$TEMPLATE"

##  $1: PAth to the file in call
##  $2: Execution path
function getFilePath() {
#  echo -e "getFilePAth:"
#  echo -e "     Primer parámetro: $1 "
#  echo -e "     Segundo parámetro: $2"
  firstCharacter="${1:0:1}"

  if [ "$firstCharacter" == "/" ]; then
    echo -e "Path completo: $1"
    FILEPATH=${1%/*}
    FILENAME=${1##*/}
    FILENAMENOEXT="${FILENAME%.*}"
  else
    echo -e "Path completo: $2/$1"
    completo="$2/$1"
    FILEPATH=${completo%/*}
    FILENAME=${completo##*/}
    FILENAMENOEXT="${FILENAME%.*}"
  fi
#  echo -e "getFilePath:"
#  echo -e "     FILEPATH:           $FILEPATH"
#  echo -e "     FILENAME:       $FILENAME"
#  echo -e "     FILENAMENOEXT:  $FILENAMENOEXT"
}

function help() {
  echo Se debe pasar como parámetro como mínimo el path al fichero que quiere transformarse de markdown a revealjs slides:
  echo       ej.  $0 ./presentacion.md
  echo       ej.  $0 /home/usuario/presentacion.md
  echo       ej.  $0 /home/usuario/presentacion.md --self-contained  # Images will be included as data URI
  exit
}

## MAIN

if [ "x$1" != "x" ]; then

  echo parámetro: $1

  if [ ! -f "$1" ]; then
    echo El parámetro no es un archivo válido. Revise la ruta al archivo
    help
  fi


  getFilePath "$1" "`pwd`"
  FILENAMENOEXT="${FILENAME%.*}"

  echo -e "Transforming "
  echo -e "   markdown file: $FILEPATH/$FILENAME "
  echo -e "   into slides $FILEPATH/${FILENAMENOEXT}_revealjs.html"
  echo -e " ................"


  /usr/bin/pandoc  -f markdown -t revealjs -s  \
  --template $markdownSlidesBaseDir/$TEMPLATE --number-sections \
  -V revealjs-url=https://unpkg.com/reveal.js@3.9.2/ \
  -V revealMenuSrc=https://asanzdiego.github.io/markdownslides/doc/lib/reveal.js-menu \
  -V theme=league \
  --variable width=1600 --variable height=900 --variable minScale=0.1 --variable maxScale=1.9 --variable margin=0.3 \
  -o "$FILEPATH/${FILENAMENOEXT}_revealjs.html" "$FILEPATH/$FILENAME"


  # 1. Eliminado --self-contained de la llamada a pandoc. Se hace con el script propio llamado image-self-contained.sh
  # 2. Debido a este error https://github.com/jgm/pandoc/issues/5725 que afecta a la versión 2.5 que se instala en Ubuntu,
  # hay que retocar las etiquetas style cuando se usa --self-contained
  # if [ "x$2" != "x" ]; then
  #   # echo "Replacing style type field..."
  #   sed -i 's/text\/css; charset=utf-8/text\/css/g' "$FILEPATH/${FILENAMENOEXT}_revealjs.html"
  # fi
  if [ "x$2" == "x--self-contained" ]; then
      env
      ${markdownSlidesBaseDir}/image-self-contained.sh "${FILEPATH}/${FILENAMENOEXT}_revealjs.html"
      if [ -f "$FILEPATH/${FILENAMENOEXT}_revealjs_self-contained.html" ];
      then
        rm "$FILEPATH/${FILENAMENOEXT}_revealjs.html"
      fi
  fi

  echo -e "HECHO."

##    --self-contained
else
  help

fi
